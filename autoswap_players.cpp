#include "autoswap_players.h"
#include "autoswap_plugin.h"

#include "interface.h"
#include "eiface.h"
#include "game/server/iplayerinfo.h"

#include "tier1/utlstring.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

bool isValidEntity(const edict_t * entity)
{
    return entity != NULL && !entity->IsFree();
}

bool isValidPlayerInfo(IPlayerInfo * pInfo)
{
    return pInfo != NULL && pInfo->IsConnected() && pInfo->IsPlayer() && !pInfo->IsHLTV();
}

void kick(IPlayerInfo * player)
{
    CUtlString cmdBuilder;
	cmdBuilder += "kickid ";
	cmdBuilder += player->GetUserID();
	cmdBuilder += "\n";

	engine->ServerCommand(cmdBuilder.Get());
}

void swapPlayer(IPlayerInfo * player)
{
	if (player->IsFakeClient())
		kick(player);
	else
	{
		switch (player->GetTeamIndex())
		{
		case TEAM_T:
			player->ChangeTeam(TEAM_CT);
			break;
		case TEAM_CT:
			player->ChangeTeam(TEAM_T);
			break;
		}
	}
}

void swapAllPlayers()
{
	for (int i = 1; i <= playerinfomanager->GetGlobalVars()->maxClients; i++)
	{
		edict_t * entity = engine->PEntityOfEntIndex(i);
		if (isValidEntity(entity))
		{
			IPlayerInfo * player = playerinfomanager->GetPlayerInfo(entity);
			if (isValidPlayerInfo(player))
				swapPlayer(player);
		}
	}
}
