#ifndef PLAYERS_H
#define PLAYERS_H

class IPlayerInfo;
struct edict_t;

// Num�ro des teams pour CS:S
/// Team terroriste
const int TEAM_T = 2;

/// Team anti-terroriste
const int TEAM_CT = 3;

/// D�termine si un pointeur de type edict_t est valide et repr�sente bien une entit� existante.
/// Retourne true si c'est le cas, false sinon.
bool isValidEntity(const edict_t * entity);

/// D�termine si un pointeur de type IPlayerInfo est valide et repr�sente bien un joueur connect�.
/// On exclut SourceTv, qui est � manipuler comme un cas particulier pour beaucoup de choses....
/// Retourne true si c'est le cas, false sinon.
bool isValidPlayerInfo(IPlayerInfo * pInfo);

/// Kick un joueur.
/// Le pointeur doit �tre valide !
void kick(IPlayerInfo * player);

/// Swappe un joueur.
/// Le pointeur doit �tre valide !
void swapPlayer(IPlayerInfo * player);

/// Swappe tous les joueurs.
void swapAllPlayers();

#endif